# hampelsoft-test-sub

## :memo: Description 

This repository is for testing the submodules feature of git (see https://git-scm.com/book/en/v2/Git-Tools-Submodules) within the hampelsoft-test repository

## :wrench: LabVIEW 2014

The VIs are maintained in LabVIEW 2014.

## :bulb: Documentation

The HSE Dokuwiki holds more general information:

* https://dokuwiki.hampel-soft.com/processes/collaboration
* https://dokuwiki.hampel-soft.com/code/common/repository-structure
* https://dokuwiki.hampel-soft.com/code/common/project-structure

## :busts_in_silhouette: Contributing 

All contents of this repository are created and provided by HAMPEL SOFTWARE ENGINEERING (HSE, www.hampel-soft.com).

Please get in touch with us at (office@hampel-soft.com) or visit our website (www.hampel-soft.com) if you want to contribute.

##  :beers: Credits

* Joerg Hampel
* Manuel Sebald

## :page_facing_up: License 

This repository and its contents are licensed under a BSD/MIT like license - see the [LICENSE](LICENSE) file for details